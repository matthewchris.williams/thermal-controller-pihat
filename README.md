# Thermal Controller PiHat
The use case of this PiHat is to monitor, regulate and control the temperature of an attached external device which contains a heating element. 
It uses a switching voltage regulator subsystem to power the Raspberry Pi and LED and Temperature sensing subsystems.

The system allows a user to connect the system to a heater 
power cable(via the onboard relay) for example and allows the user to specify
an exact temperature which allows extremely customised temperature control in
contrast to adjusting the temperature with a control knob as with most generic
heaters. This allows for a room temperature to be decently maintained.

